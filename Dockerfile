FROM python:3.11

ARG POETRY_VERSION=1.4.2

RUN pip install "poetry~=${POETRY_VERSION}" && \
    poetry config virtualenvs.create false

WORKDIR /app
COPY poetry.lock pyproject.toml README.md ./
COPY src src

RUN poetry install

ENTRYPOINT ["example"]
