from . import data


def entrypoint():
    important_data = data.get_important_data()

    print("Top 3 Pink Floyd Albums")
    print("======================")

    for item in important_data:
        print(item)
