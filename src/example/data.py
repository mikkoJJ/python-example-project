"""The data class handles important data generation.
"""


def get_important_data() -> str:
    return [
        "Dark Side of the Moon",
        "Animals",
        "Wish You Were Here",
    ]
