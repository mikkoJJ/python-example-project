from example import data
import pytest


@pytest.fixture
def important_data():
    return data.get_important_data()


def test_has_wywh(important_data):
    assert "Wish You Were Here" in important_data


def test_has_animals(important_data):
    assert "Animals" in important_data


def test_has_dotm(important_data):
    assert "Dark Side of the Moon" in important_data
